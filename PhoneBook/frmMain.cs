﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneBook
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private DataSet ds = new DataSet();

        private void Form1_Load(object sender, EventArgs e)
        {
            //Загрузка таблицы
            reloadbase();

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void mnuRefresh_Click(object sender, EventArgs e)
        {
            reloadbase();
        }

        private void btnNewRegister_Click(object sender, EventArgs e)
        {
            frmAdd frm = new frmAdd();
            DialogResult sonuc = frm.ShowDialog();

            if (sonuc == System.Windows.Forms.DialogResult.OK)
            {
                man newman = frm.newman;

                DataTable dt = this.dataGridView1.DataSource as DataTable;

                DataRow dr = dt.NewRow();
                dr[0] = newman.Id;
                dr[1] = newman.Surname;
                dr[2] = newman.fname;
                dr[3] = newman.patronymic;
                dr[4] = newman.TelefonNo;
                dr[5] = newman.adress;
                dr[6] = newman.state;
                dr[7] = newman.posit;
                dt.Rows.Add(dr);

                this.RegistrationNumberCalculation();

                this.LatestTransactionInformation.Text = "Добавлена ​​новая запись.";

            }

        }

        private void btnRegisterEdit_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.CurrentCell != null)
            {
               
                frmEdit frm = new frmEdit();
                frm.refreshman = new man();
                frm.refreshman.Surname = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
                frm.refreshman.fname = this.dataGridView1.CurrentRow.Cells[2].Value.ToString();
                frm.refreshman.patronymic = this.dataGridView1.CurrentRow.Cells[3].Value.ToString();
                frm.refreshman.TelefonNo = this.dataGridView1.CurrentRow.Cells[4].Value.ToString();
                frm.refreshman.adress = this.dataGridView1.CurrentRow.Cells[5].Value.ToString();
                frm.refreshman.state = this.dataGridView1.CurrentRow.Cells[6].Value.ToString();
                frm.refreshman.posit = this.dataGridView1.CurrentRow.Cells[7].Value.ToString();
                DialogResult sonuc = frm.ShowDialog();

                if (sonuc == DialogResult.OK)
                {
                    DataRowView drv = this.dataGridView1.CurrentRow.DataBoundItem as DataRowView;
                    DataRow dr = drv.Row;

                    dr[1] = frm.refreshman.Surname;
                    dr[2] = frm.refreshman.fname;
                    dr[3] = frm.refreshman.patronymic;
                    dr[4] = frm.refreshman.TelefonNo;
                    dr[5] = frm.refreshman.adress;
                    dr[6] = frm.refreshman.state;
                    dr[7] = frm.refreshman.posit;
                    this.LatestTransactionInformation.Text = "Запись сделана";

                }
            }
        }

        private void btndel_record_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.CurrentRow != null)
            {
                DialogResult sonuc = MessageBox.Show("Вы уверены, что хотите удалить выбранную запись?", "Удалить запись", MessageBoxButtons.YesNoCancel);

                if (sonuc == DialogResult.Yes)
                {
                    DataRowView drv = this.dataGridView1.CurrentRow.DataBoundItem as DataRowView;
                    DataRow dr = drv.Row;

                    dr.Delete();

                    this.RegistrationNumberCalculation();

                    this.LatestTransactionInformation.Text = "Запись удалена.";
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ds.WriteXml(Application.StartupPath + "\\" + "data.xml", XmlWriteMode.WriteSchema);
            this.LatestTransactionInformation.Text = "Данные были записаны.";
        }

        private void tmrhour_Tick(object sender, EventArgs e)
        {
            this.lblTime.Text = DateTime.Now.ToShortTimeString();
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            ds.WriteXml(Application.StartupPath + "\\" + "data.xml", XmlWriteMode.WriteSchema);
            Application.Exit();
        }

        private void RegistrationNumberCalculation()
        {
            this.lblRegistrations.Text = "Количество записей:" + this.dataGridView1.RowCount.ToString();
            
        }

        private void reloadbase()
        {
            ds.Tables.Clear();
            ds.ReadXml(Application.StartupPath + "\\" + "data.xml", XmlReadMode.ReadSchema);

            if (ds.Tables.Count > 0)
            {
                this.dataGridView1.DataSource = ds.Tables[0];
                this.RegistrationNumberCalculation();

                this.LatestTransactionInformation.Text = "Данные загружены.";
            }
        }

        private void mnuProgramInformation_Click(object sender, EventArgs e)
        {
            frmSpravka frm = new frmSpravka();
            frm.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void mnuYardim_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
        String.Format("Имя like '{0}%'", txtfname.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
        String.Format("Фамилия like '{0}%'", txtfname.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
String.Format("Телефон like '{0}%'", txtfname.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtfname.Text = "";
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
String.Format("Id like '{0}%'", txtfname.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
String.Format("Адрес like '{0}%'", txtfname.Text);
        }
        private void button6_Click(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
String.Format("Факультет like '{0}%'", txtfname.Text);
        }
        private void button7_Click(object sender, EventArgs e)
        {
            (dataGridView1.DataSource as DataTable).DefaultView.RowFilter =
String.Format("Должность like '{0}%'", txtfname.Text);
        }
    }
}
