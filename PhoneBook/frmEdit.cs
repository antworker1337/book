﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneBook
{
    public partial class frmEdit : Form
    {
        public man refreshman { get; set; }

        public frmEdit()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.refreshman.fname = txt_name.Text.ToString();
            this.refreshman.Surname = txt_surname.Text.ToString();
            this.refreshman.patronymic = txt_patronymic.Text.ToString();
            this.refreshman.TelefonNo = txt_phone.Text.ToString();
            this.refreshman.adress = txt_adress.Text.ToString();
            this.refreshman.state = txt_state.Text.ToString();
            this.refreshman.posit = txt_posit.Text.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void frmDuzenle_Load(object sender, EventArgs e)
        {
            if (this.refreshman != null)
            {
                this.txt_name.Text = this.refreshman.fname;
                this.txt_surname.Text = this.refreshman.Surname;
                this.txt_patronymic.Text = this.refreshman.patronymic;
                this.txt_phone.Text = this.refreshman.TelefonNo;
                this.txt_adress.Text = this.refreshman.adress;
                this.txt_state.Text = this.refreshman.state;
                this.txt_posit.Text = this.refreshman.posit;
            }
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            TextBox txt = this.ActiveControl as TextBox;

            if (txt != null)
            {
                txt.Cut();
            }
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            TextBox txt = this.ActiveControl as TextBox;

            if (txt != null)
            {
                txt.Copy();
            }
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            TextBox txt = this.ActiveControl as TextBox;

            if (txt != null)
            {
                txt.Paste();
            }
        }
    }
}
