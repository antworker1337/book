﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneBook
{
    public partial class frmAdd : Form
    {

        public man newman { get; set; }

        public frmAdd()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.newman = new man();

            
            this.newman.fname = txt_name.Text.ToString();
            this.newman.Surname = txt_surname.Text.ToString();
            this.newman.patronymic = txt_patronymic.Text.ToString();
            this.newman.TelefonNo = txt_phone.Text.ToString();
            this.newman.adress = txt_adress.Text.ToString();
            this.newman.state = txt_state.Text.ToString();
            this.newman.posit = txt_posit.Text.ToString();

            string[] man = new string[8];

            System.IO.File.Create(Application.StartupPath + @"\records.txt");

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            TextBox txt = this.ActiveControl as TextBox;

            if (txt != null)
            {
                txt.Cut();
            }
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            TextBox txt = this.ActiveControl as TextBox;

            if (txt != null)
            {
                txt.Copy();
            }
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            TextBox txt = this.ActiveControl as TextBox;

            if (txt != null)
            {
                txt.Paste();
            }
        }

        private void frmAdd_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        
    }
}
