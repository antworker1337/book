﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


[assembly: AssemblyTitle("Телефонный Справочник")]
[assembly: AssemblyDescription("Телефонный справочник на C# с использованием Windows Forms ")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("КНиИТ")]
[assembly: AssemblyProduct("PhoneBook")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("КНиИТ")]
[assembly: AssemblyCulture("")]


[assembly: ComVisible(false)]


[assembly: Guid("68054464-fc97-417c-a4d5-4d5ee7c8a00a")]


[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguage("tr")]

